# import csv 
# import matplotlib.pyplot as plt
# from datetime import datetime
# from collections import Counter

# file_path = 'Maharashtra.csv'


# registration_years = []

# with open(file_path, 'r', encoding = 'ISO-8859-1') as file:
#     csv_reader = csv.DictReader(file)
#     for row in csv_reader:
#          # Assuming 'DATE_OF_REGISTRATION' is the column name and it is in the format 'B-m-Y'
#         date_of_registration = row['DATE_OF_REGISTRATION']
        
#         # Skip rows with 'NA' or empty values
#         if date_of_registration.upper() == 'NA' or not date_of_registration:
#             continue
        
#         # Extract year only if the last two characters are digits
#         if date_of_registration[-2:].isdigit():
#             registration_year = int(date_of_registration[-2:])
#             registration_years.append(registration_year)
        
# year_counts = Counter(registration_years)
# print(year_counts)

# year = list(year_counts.keys())

# counts =  list(year_counts.values())
# plt.bar(year, counts, color = 'red')
# plt.title('Company Registration by Years')        
# plt.xlabel('Years of Registration')
# plt.ylabel('Number of Registration')
# plt.xticks(year)        
# plt.show()
import csv
from collections import Counter
import matplotlib.pyplot as plt


def calculate_registration_years(file_path):
    registration_years = []

    with open(file_path, 'r', encoding='ISO-8859-1') as file:
        csv_reader = csv.DictReader(file)
        for row in csv_reader:
            # Assuming 'DATE_OF_REGISTRATION' is the column name and it is in the format 'B-m-Y'
            date_of_registration = row['DATE_OF_REGISTRATION']

            # Skip rows with 'NA' or empty values
            if date_of_registration.upper() == 'NA' or not date_of_registration:
                continue

            # Extract year only if the last two characters are digits
            if date_of_registration[-2:].isdigit():
                registration_year = int(date_of_registration[-2:])
                registration_years.append(registration_year)

    year_counts = Counter(registration_years)
    return year_counts


def plot_registration_years(year_counts):
    year = list(year_counts.keys())
    counts = list(year_counts.values())

    plt.bar(year, counts, color='red')
    plt.title('Company Registration by Years')
    plt.xlabel('Years of Registration')
    plt.ylabel('Number of Registration')
    plt.xticks(year)
    plt.show()


def execute(file_path):
    year_counts = calculate_registration_years(file_path)
    plot_registration_years(year_counts)

# Execute the program


file_path = 'Maharashtra.csv'
execute(file_path)
