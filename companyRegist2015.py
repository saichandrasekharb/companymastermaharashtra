# import csv

# import matplotlib.pyplot as plt

# pincode_obj ={}


# with open('Maharashtra.csv', encoding='unicode_escape') as file_obj: 
      
#     heading = next(file_obj)
#     heading = heading[:len(heading)-1]
#     a = heading.split(",")
#     columns = {}

#     for i in range (0, len(a)): columns[a[i]]=i

#     reader_obj = csv.reader(file_obj) 
      
#     for row in reader_obj:
#         b=row[columns['Registered_Office_Address']]
#         lis = list(b.split(" "))
#         pincode=lis[len(lis)-1]
#         # print(lis[len(lis)-1])
#         if((pincode=='') | (row[columns['DATE_OF_REGISTRATION']][-2:]!='15')): continue
#         pincode_obj[pincode] = pincode_obj.get(pincode, 0) + 1
#         obj = {}
# with open('zipcode(1).csv', encoding='unicode_escape') as file_obj:
#     heading = next(file_obj)
#     heading = heading[:len(heading)-1]
#     a = heading.split(",")
#     columns = {}

#     for i in range (0, len(a)): columns[a[i]]=i

#     reader_obj = csv.reader(file_obj) 
      
#     for row in reader_obj:
#         obj[row[columns['Pincode']]]=row[columns['District']]
#         p = []
# q = []
# for i in pincode_obj:
#     if i in obj.keys(): 
#         # print(obj[i])
#         p.append(obj[i])
#         q.append(pincode_obj[i])
#         # print(type(pincode_obj[i]))

# # print(p)

# plt.barh(p, q)

import csv
import matplotlib.pyplot as plt


def calculate_pincode_counts(file_path, registration_year='15'):
    pincode_obj = {}
    obj = {}

    with open(file_path, encoding='unicode_escape') as file_obj:
        heading = next(file_obj)
        heading = heading[:len(heading)-1]
        a = heading.split(",")
        columns = {}

        for i in range(0, len(a)):
            columns[a[i]] = i

        reader_obj = csv.reader(file_obj)

        for row in reader_obj:
            b = row[columns['Registered_Office_Address']]
            lis = list(b.split(" "))
            pincode = lis[len(lis)-1]

            if (pincode == '') or (row[columns['DATE_OF_REGISTRATION']][-2:] != registration_year):
                continue

            pincode_obj[pincode] = pincode_obj.get(pincode, 0) + 1

    with open('zipcode(1).csv', encoding='unicode_escape') as file_obj:
        heading = next(file_obj)
        heading = heading[:len(heading)-1]
        a = heading.split(",")
        columns = {}

        for i in range(0, len(a)):
            columns[a[i]] = i

        reader_obj = csv.reader(file_obj)

        for row in reader_obj:
            obj[row[columns['Pincode']]] = row[columns['District']]

    return pincode_obj, obj


def plot_pincode_counts(pincode_obj, obj):
    p = []
    q = []

    for i in pincode_obj:
        if i in obj.keys():
            p.append(obj[i])
            q.append(pincode_obj[i])

    plt.barh(p, q)
    plt.show()


def execute():
    file_path = 'Maharashtra.csv'
    registration_year = '15'

    pincode_obj, obj = calculate_pincode_counts(file_path, registration_year)
    plot_pincode_counts(pincode_obj, obj)

# Execute the program


execute()


execute()


execute()