# import csv
# import matplotlib.pyplot as plt
# import numpy as np


# obj = {}
# labels = {}
# with open('Maharashtra.csv', encoding='unicode_escape') as file_obj:
#     heading = next(file_obj)
#     heading = heading[:len(heading)-1]
#     a = heading.split(",")
#     columns = {}

#     for i in range (0, len(a)):
#         columns[a[i]] = i

#     reader_obj = csv.reader(file_obj)
    
#     for row in reader_obj:
#         b = row[columns['DATE_OF_REGISTRATION']][-2:]
#         if(b=="NA"):
#             continue
#         c = (int)(b)
#         if(( c <=11) or ( c >21)):
#             continue
#         if c not in obj:
#             obj[c]={}

#         d=row[columns['PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN']]
#         labels[d]=labels.get(d, 0)+1
#         # array.append(d)
#         obj[c][d]=obj[c].get(d, 0)+1

# labels=dict(sorted(labels.items(), key = lambda x: x[1], reverse = True))
# labels=dict(list(labels.items())[0: 5])
# arr2d = []

# for i in labels:
#     tmp = []
#     for j in obj:
#         # print(obj[j][i])
#         tmp.append(obj[j][i])
#     arr2d.append(tmp)

# # print(arr2d)
# dir = [-0.4, -0.2, 0.0, 0.2 ,0.4]
# plt.figure(figsize=(12,10))
# for i in range (0,5):
#      plt.barh(np.array(list(obj.keys()))+dir[i], arr2d[i], 0.2)
# plt.legend(labels.keys())
# plt.show() 

import csv
import matplotlib.pyplot as plt
import numpy as np


def calculate_data(file_path):
    obj = {}
    labels = {}

    with open(file_path, encoding='unicode_escape') as file_obj:
        heading = next(file_obj)
        heading = heading[:len(heading) - 1]
        columns = {}

        for i in range(0, len(heading.split(","))):
            columns[heading.split(",")[i]] = i

        reader_obj = csv.reader(file_obj)

        for row in reader_obj:
            b = row[columns['DATE_OF_REGISTRATION']][-2:]
            if b == "NA":
                continue
            c = int(b)
            if (c <= 11) or (c > 21):
                continue
            if c not in obj:
                obj[c] = {}

            d = row[columns['PRINCIPAL_BUSINESS_ACTIVITY_AS_PER_CIN']]
            labels[d] = labels.get(d, 0) + 1
            obj[c][d] = obj[c].get(d, 0) + 1

    labels = dict(sorted(labels.items(), key=lambda x: x[1], reverse=True))
    labels = dict(list(labels.items())[:5])

    return obj, labels


def plot_data(obj, labels):
    arr2d = []

    for i in labels:
        tmp = []
        for j in obj:
            tmp.append(obj[j][i])
        arr2d.append(tmp)

    dir = [-0.4, -0.2, 0.0, 0.2, 0.4]
    plt.figure(figsize=(12, 10))
    for i in range(0, 5):
        plt.barh(np.array(list(obj.keys())) + dir[i], arr2d[i], 0.2)
    plt.legend(labels.keys())
    plt.show()


def execute(file_path):
    obj, labels = calculate_data(file_path)
    plot_data(obj, labels)

# Execute the program


file_path = 'Maharashtra.csv'
execute(file_path)


