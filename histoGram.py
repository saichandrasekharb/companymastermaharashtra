import csv
import matplotlib.pyplot as plt


def calculate_authorized_capital(file_path):
    authorized_cap_values = []

    with open(file_path, 'r', encoding='latin-1') as csvfile:
        data_reader = csv.DictReader(csvfile)
        for row in data_reader:
            value = float(row.get('AUTHORIZED_CAP', '0'))
            authorized_cap_values.append(value)

    interval_counts = {'<= 1l': 0, '1L to 10L': 0, '10L to 1Cr': 0, '1Cr to 10Cr': 0, '> 10Cr': 0}

    for cap in authorized_cap_values:
        if cap <= 100000:
            interval_counts['<= 1l'] += 1
        elif cap <= 1000000:
            interval_counts['1L to 10L'] += 1
        elif cap <= 10000000:
            interval_counts['10L to 1Cr'] += 1
        elif cap <= 100000000:
            interval_counts['1Cr to 10Cr'] += 1
        else:
            interval_counts['> 10Cr'] += 1

    return interval_counts


def plot_authorized_capital(interval_counts):
    plt.bar(interval_counts.keys(), interval_counts.values(), color='green', alpha=0.5, width=0.5)
    plt.xlabel('Authorized Capital Intervals')
    plt.ylabel('Frequency')
    plt.title('Histogram of Authorized Capital')
    plt.xticks(rotation=0, ha='right')
    plt.tight_layout()
    plt.show()


def execute(file_path):
    interval_counts = calculate_authorized_capital(file_path)
    plot_authorized_capital(interval_counts)

# Execute the program


file_path = 'Maharashtra.csv'
execute(file_path)




# import csv
# import matplotlib.pyplot as plt

# def Authorized_Capital():
#     # Initialize an empty list for 'AUTHORIZED_CAP' values
#     authorized_cap_values = []
#     # intervals = [0, 100000, 1000000, 10000000, 100000000, float('inf')]
#     # labels = ['<= 1L', '1L to 10L', '10L to 1Cr', '1Cr to 10Cr', '> 10Cr']

#     # Read the CSV file and extract 'AUTHORIZED_CAP' values using a for loop
#     with open('Maharashtra.csv', 'r', encoding='latin-1') as csvfile:
#         data_reader = csv.DictReader(csvfile)
#         for row in data_reader:
#             value = float(row.get('AUTHORIZED_CAP', '0'))
#             authorized_cap_values.append(value)


#     interval_counts ={'<= 1l':0 , '1L to 10L': 0, '10L to 1Cr': 0, '1Cr to 10Cr': 0, '> 10Cr': 0}

#     for cap in authorized_cap_values:
#         if cap <= 100000:
#             interval_counts['<= 1l']+=1
#         elif cap <= 1000000:
#             interval_counts['1L to 10L']+=1
#         elif cap <= 10000000:
#             interval_counts['10L to 1Cr']+=1
#         elif cap <= 100000000:
#             interval_counts['1Cr to 10Cr']+=1
#         else:
#             interval_counts['> 10Cr']+= 1

#     plt.bar(interval_counts.keys() , interval_counts.values() , color = 'green' , alpha =0.5 , width = 0.5 )
#     plt.xlabel('Authorized Capital Intervals')
#     plt.ylabel('Frequency')
#     plt.title('Histogram of Authorized Capital')
#     plt.xticks(rotation=0, ha='right')
#     plt.tight_layout()
#     plt.show()

